/*
 Copyright (c) 2011-2013, HL7, Inc.
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of HL7 nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific
 prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

//
//  FHIRFeedSerializeOrderPair.m
//  FHIR Test
//
//  Created by Andrew on 2/10/2014.
//  Copyright (c) 2014 Ideaworks. All rights reserved.
//

#import "FHIRFeedSerializerOrderPair.h"

@interface FHIRFeedSerializerOrderPair ()

+ (NSArray *)orderPairArrayForFHIRFeed;
+ (NSArray *)orderPairArrayForFHIREntry;
+ (NSArray *)orderPairArrayForFHIRAuthor;
+ (NSArray *)orderPairArrayForFHIRSummary;
+ (NSArray *)orderPairArrayForFHIRContent;
+ (NSArray *)orderPairArrayForFHIRLink;
+ (NSArray *)orderPairArrayForFHIRTotalResults;

@end

@implementation FHIRFeedSerializerOrderPair

+ (NSArray *)orderPairArrayForFHIRFeed
{
    return @[
             @[@"title",@"NSString",],
             @[@"id",@"FHIRUri",],
             @[@"link",@"FHIRLink",],
             @[@"updated",@"NSDate",],
             @[@"totalResults",@"FHIRTotalResults",],
             @[@"entry",@"FHIREntry",],
             ];
}

+ (NSArray *)orderPairArrayForFHIREntry
{
    return @[
             @[@"title",@"NSString",],
             @[@"id",@"FHIRUri",],
             @[@"link",@"FHIRLink",],
             @[@"updated",@"NSDate",],
             @[@"author",@"FHIRAuthor",],
             @[@"published",@"NSDate",],
             @[@"content",@"FHIRContent",],
             @[@"summary",@"FHIRSummary",],
             ];
}

+ (NSArray *)orderPairArrayForFHIRAuthor
{
    return @[
             @[@"name",@"NSString",],
             ];
}

+ (NSArray *)orderPairArrayForFHIRSummary
{
    return @[
             @[@"div",@"NSString",],
             ];
}

+ (NSArray *)orderPairArrayForFHIRContent
{
    return @[
             @[@"type",@"NSString",],
             @[@"fhirObject",@"id",],
             ];
}

+ (NSArray *)orderPairArrayForFHIRLink
{
    return @[
             @[@"href",@"NSString",],
             @[@"rel",@"NSString",],
             ];
}

+ (NSArray *)orderPairArrayForFHIRTotalResults
{
    return @[
             @[@"xmnls",@"NSString",],
             @[@"value",@"NSString",],
             ];
}


+ (NSArray *)orderPairArrayForType:(NSString *)fhirOcType {
    
    if([fhirOcType isEqualToString:@"FHIRFeed"])
    {
        return [self orderPairArrayForFHIRFeed];
    }
    else if([fhirOcType isEqualToString:@"FHIREntry"])
    {
        return [self orderPairArrayForFHIREntry];
    }
    else if([fhirOcType isEqualToString:@"FHIRAuthor"])
    {
        return [self orderPairArrayForFHIRAuthor];
    }
    else if([fhirOcType isEqualToString:@"FHIRSummary"])
    {
        return [self orderPairArrayForFHIRSummary];
    }
    else if([fhirOcType isEqualToString:@"FHIRContent"])
    {
        return [self orderPairArrayForFHIRContent];
    }
    else if([fhirOcType isEqualToString:@"FHIRLink"])
    {
        return [self orderPairArrayForFHIRLink];
    }
    else if([fhirOcType isEqualToString:@"FHIRTotalResults"])
    {
        return [self orderPairArrayForFHIRTotalResults];
    }
    else
        return nil;
}

@end
