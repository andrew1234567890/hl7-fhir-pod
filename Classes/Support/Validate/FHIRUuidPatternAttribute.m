/*
 Copyright (c) 2011-2013, HL7, Inc.
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of HL7 nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific
 prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

//
//  FHIRUuidPatternAttribute.m
//  FHIR Test
//
//  Created by Andrew on 2/3/2014.
//  Copyright (c) 2014 Ideaworks. All rights reserved.
//

#import "FHIRUuidPatternAttribute.h"
#import "FHIRValidationResult.h"
#import "FHIRUuid.h"

@implementation FHIRUuidPatternAttribute

- (FHIRValidationResult *)isValidValue:(id)value {
    
    if (value == nil) {
        return [[FHIRValidationResult alloc] initWithSuccess];
    }
    
    if (![value isKindOfClass:[NSString class]]) {
        @throw [NSException exceptionWithName:@"ArgumentException" reason:@"UuidPatternAttribute can only be applied to string properties." userInfo:nil];
    }
    
    if ([FHIRUuidPatternAttribute isValidString:value]) {
        return [[FHIRValidationResult alloc] initWithSuccess];
    } else {
        return [[FHIRValidationResult alloc] initWithFailureMessage:@"Not a correctly formatted Uuid."];
    }
}

+ (BOOL)isValidString:(NSString *)value {
    
    NSString *pattern = [NSString stringWithFormat:@"^%@$",PATTERN_UUID];
    
    NSError *error;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    
    return [regex numberOfMatchesInString:value options:0 range:NSMakeRange(0, [value length])];
}

@end
