﻿/*
  Copyright (c) 2011-2013, HL7, Inc.
  All rights reserved.
  
  Redistribution and use in source and binary forms, with or without modification, 
  are permitted provided that the following conditions are met:
  
   * Redistributions of source code must retain the above copyright notice, this 
     list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, 
     this list of conditions and the following disclaimer in the documentation 
     and/or other materials provided with the distribution.
   * Neither the name of HL7 nor the names of its contributors may be used to 
     endorse or promote products derived from this software without specific 
     prior written permission.
  
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
  POSSIBILITY OF SUCH DAMAGE.
  

 * Generated on Thu, Feb 13, 2014 10:59-0500 for FHIR v0.80
 */
/*
 * A request for a diagnostic service
 */
#import "FHIRDiagnosticOrder.h"

#import "FHIRResourceReference.h"
#import "FHIRIdentifier.h"
#import "FHIRString.h"
#import "FHIRCode.h"
#import "FHIRDiagnosticOrderEventComponent.h"
#import "FHIRDiagnosticOrderItemComponent.h"

@implementation FHIRDiagnosticOrder

- (NSString *)clinicalNotes
{
    if(self.clinicalNotesElement)
    {
        return [self.clinicalNotesElement value];
    }
    return nil;
}

- (void )setClinicalNotes:(NSString *)clinicalNotes
{
    if(clinicalNotes)
    {
        [self setClinicalNotesElement:[[FHIRString alloc] initWithValue:clinicalNotes]];
    }
    else
    {
        [self setClinicalNotesElement:nil];
    }
}


- (kDiagnosticOrderStatus )status
{
    return [FHIREnumHelper parseString:[self.statusElement value] enumType:kEnumTypeDiagnosticOrderStatus];
}

- (void )setStatus:(kDiagnosticOrderStatus )status
{
    [self setStatusElement:[[FHIRCode/*<code>*/ alloc] initWithValue:[FHIREnumHelper enumToString:status enumType:kEnumTypeDiagnosticOrderStatus]]];
}


- (kDiagnosticOrderPriority )priority
{
    return [FHIREnumHelper parseString:[self.priorityElement value] enumType:kEnumTypeDiagnosticOrderPriority];
}

- (void )setPriority:(kDiagnosticOrderPriority )priority
{
    [self setPriorityElement:[[FHIRCode/*<code>*/ alloc] initWithValue:[FHIREnumHelper enumToString:priority enumType:kEnumTypeDiagnosticOrderPriority]]];
}



@end
