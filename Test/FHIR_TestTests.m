//
//  FHIR_TestTests.m
//  FHIR TestTests
//
//  Created by Andrew on 2014-01-27.
//  Copyright (c) 2014 Ideaworks. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FHIRParser.h"
#import "FHIRXmlWriter.h"
#import "FHIRJsonWriter.h"

#import "FHIRPatient.h"
#import "FHIRHumanName.h"
#import "FHIRIdentifier.h"
#import "FHIRCodeableConcept.h"
#import "FHIRCoding.h"

#import "FHIRPost.h"

#import "FHIRFeed.h"
#import "FHIREntry.h"
#import "FHIRContent.h"

#import "FHIRModelValidator.h"

@interface FHIR_TestTests : XCTestCase

@property (nonatomic, strong) FHIRPost *post;
@property (nonatomic) BOOL loud;

@end

@implementation FHIR_TestTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    [self setPost:[FHIRPost new]];
    [self setLoud:YES];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPatientValidation{
    
    NSLog(@"Patient 106 Validate... start");
    
    NSString *result = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Patient xmlns=\"http://hl7.org/fhir\"><text><status value=\"generated\"/><div xmlns=\"http://www.w3.org/1999/xhtml\">West, James. MRN:&#x0A;            577425</div></text><identifier><label value=\"SSN\"/><system value=\"https://github.com/projectcypress/cypress/patient\"/><value value=\"577425\"/></identifier><name><use value=\"official\"/><family value=\"West\"/><given value=\"James\"/></name><gender><coding><system value=\"http://hl7.org/fhir/v3/AdministrativeGender\"/><code value=\"M\"/><display value=\"Male\"/></coding></gender><birthDate value=\"1946-06-08\"/><managingOrganization><reference value=\"Organization/1\"/></managingOrganization><active value=\"true\"/></Patient>";
    
    
    FHIRPatient *patient = [FHIRParser mapInfoFromXMLString:result];
    NSArray *errors = [FHIRModelValidator validateForErrors:patient];
    
    XCTAssert([errors count] == 0, @"There are %d errors reported.", [errors count]);
    
    NSLog(@"Patient 106 Test - XML... finish");
    
}

- (void)testPatientCallXML{
    
    NSLog(@"Patient 106 Test - XML... start");
    
    NSURL *url = [NSURL URLWithString:@"http://hl7connect.healthintersections.com.au/open/patient/106?_format=xml"];
    NSString *result = [self requestUrl:url];
    
    //    NSString *result = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?><Patient xmlns=\"http://hl7.org/fhir\"><text><status value=\"generated\"/><div xmlns=\"http://www.w3.org/1999/xhtml\">West, James. MRN:&#x0A;            577425</div></text><identifier><label value=\"SSN\"/><system value=\"https://github.com/projectcypress/cypress/patient\"/><value value=\"577425\"/></identifier><name><use value=\"official\"/><family value=\"West\"/><given value=\"James\"/></name><gender><coding><system value=\"http://hl7.org/fhir/v3/AdministrativeGender\"/><code value=\"M\"/><display value=\"Male\"/></coding></gender><birthDate value=\"1946-06-08\"/><managingOrganization><reference value=\"Organization/1\"/></managingOrganization><active value=\"true\"/></Patient>";
    //    NSLog(@"%@", result);
    
    FHIRPatient *patient = [FHIRParser mapInfoFromXMLString:result];
    id value;
    
    value = [(FHIRIdentifier *)[patient.identifier objectAtIndex:0] value];
    NSLog(@"Identifier Value: %@", value);
    XCTAssert([value isEqualToString:@"577425"], @"Id should match.");
    
    value = [[(FHIRHumanName *)[patient.name objectAtIndex:0] family] objectAtIndex:0];
    NSLog(@"Family Name: %@", value);
    XCTAssert([value isEqualToString:@"West"], @"Family Names should be equal.");
    
    value = [(FHIRCoding *)[patient.gender.coding objectAtIndex:0] code];
    NSLog(@"Gender Code: %@", value);
    XCTAssert([value isEqualToString:@"M"], @"Gender codes should be equal.");
    
    if (self.loud){
        
        FHIRXmlWriter *xwriter = [[FHIRXmlWriter alloc] initWithFhirObject:patient];
        NSLog(@"%@", [xwriter serializedString]);
    }
    
    NSLog(@"Patient 106 Test - XML... finish");
}

- (void)testPatientCallJSON{
    
    NSLog(@"Patient 106 Test - JSON... start");
    
    NSURL *url = [NSURL URLWithString:@"http://hl7connect.healthintersections.com.au/open/patient/106?_format=json"];
    NSString *result = [self requestUrl:url];
    
    FHIRPatient *patient = [FHIRParser mapInfoFromJSONString:result forOcType:@"FHIRPatient"];
    id value;
    
    value = [(FHIRIdentifier *)[patient.identifier objectAtIndex:0] value];
    NSLog(@"Identifier Value: %@", value);
    XCTAssert([value isEqualToString:@"577425"], @"Id should match.");
    
    value = [[(FHIRHumanName *)[patient.name objectAtIndex:0] family] objectAtIndex:0];
    NSLog(@"Family Name: %@", value);
    XCTAssert([value isEqualToString:@"West"], @"Family Names should be equal.");
    
    value = [(FHIRCoding *)[patient.gender.coding objectAtIndex:0] code];
    NSLog(@"Gender Code: %@", value);
    XCTAssert([value isEqualToString:@"M"], @"Gender codes should be equal.");
    
    if (self.loud){
        
//        FHIRJsonWriter *jwriter = [[FHIRJsonWriter alloc] initWithFhirObject:patient];
//        NSLog(@"%@", [jwriter serializedString]);
    }
    
    NSLog(@"Patient 106 Test - JSON... finish");
}

- (void)testPateintFeedXML{
    
    NSLog(@"Patient Feed Test - Local... start");
    
    NSString *result = @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>    <feed xmlns=\"http://www.w3.org/2005/Atom\">    <title>Search results for resource type Patient</title>        <id>urn:uuid:66e76ed4-dd7f-4848-a1be-414b28014c</id>        <link href=\"http://fhir.healthintersections.com.au/open/\" rel=\"fhir-base\" />        <link href=\"http://fhir.healthintersections.com.au/open/Patient/_search?_format=text/xml+fhir&amp;search-id=7d039bb1-7067-46e3-95a2-f634d85e8d&amp;search-sort=_id\" rel=\"self\" />        <link href=\"http://fhir.healthintersections.com.au/open/Patient/_search?_format=text/xml+fhir&amp;search-id=7d039bb1-7067-46e3-95a2-f634d85e8d&amp;search-sort=_id&amp;search-offset=0&amp;_count=50\" rel=\"first\" />        <link href=\"http://fhir.healthintersections.com.au/open/Patient/_search?_format=text/xml+fhir&amp;search-id=7d039bb1-7067-46e3-95a2-f634d85e8d&amp;search-sort=_id&amp;search-offset=50&amp;_count=50\" rel=\"next\" />        <link href=\"http://fhir.healthintersections.com.au/open/Patient/_search?_format=text/xml+fhir&amp;search-id=7d039bb1-7067-46e3-95a2-f634d85e8d&amp;search-sort=_id&amp;search-offset=369&amp;_count=50\" rel=\"last\" />        <updated>2014-02-10T17:33:08Z</updated>        <totalResults xmlns=\"http://a9.com/-/spec/opensearch/1.1/\">419</totalResults>        <entry xmlns=\"http://www.w3.org/2005/Atom\">        <title>Patient \"000241272\" Version \"1\"</title>        <id>http://fhir.healthintersections.com.au/open/Patient/000241272</id>        <link href=\"http://fhir.healthintersections.com.au/open/Patient/000241272/_history/1\" rel=\"self\" />        <updated>2014-01-16T22:51:36Z</updated>        <author>        <name>174.76.81.250</name>        </author>        <published>2014-02-10T17:33:08Z</published>        <content type=\"text/xml\">        <Patient xmlns=\"http://hl7.org/fhir\">        <text>        <status value=\"generated\"/>        <div xmlns=\"http://www.w3.org/1999/xhtml\">Patient HEATHER  CHEN (00234321): Female, born 19880811.&#x0A;      &#x0A;      &#x0A;    <br/>Address: 4491 HILL HAVEN DRIVE, NEW ORLEANS LA 70334</div>    </text>    <identifier>    <use value=\"usual\"/>    <label value=\"mrn\"/>    <system value=\"http://www.health.acme.com/GENHOS/patients\"/>    <value value=\"00234321\"/>    </identifier>    <identifier>    <use value=\"official\"/>    <label value=\"ssn\"/>    <system value=\"urn:hl7-org:sid/us-ssn\"/>    <value value=\"436-38-7333\"/>    </identifier>    <identifier>    <label value=\"account\"/>    <system value=\"http://www.health.acme.com/accounts\"/>    <value value=\"436-38-7333\"/>    </identifier>    <name>    <text value=\"HEATHER  CHEN\"/>    <family value=\"CHEN\"/>    <given value=\"HEATHER\"/>    </name>    <telecom>    <system value=\"phone\"/>    <value value=\"504-321-2072\"/>    <use value=\"home\"/>    </telecom>    <telecom>    <system value=\"phone\"/>    <use value=\"work\"/>    </telecom>    <gender>    <coding>    <system value=\"http://hl7.org/fhir/v3/AdministrativeGender\"/>    <code value=\"F\"/>    <display value=\"Female\"/>    </coding>    <text value=\"F\"/>    </gender>    <address>    <text value=\"4491 HILL HAVEN DRIVE\r\nNEW ORLEANS LA 70334\"/>    <line value=\"4491 HILL HAVEN DRIVE\"/>    <city value=\"NEW ORLEANS\"/>    <state value=\"LA\"/>    <zip value=\"70334\"/>    </address>    <maritalStatus>    <coding>    <system value=\"http://hl7.org/fhir/v3/MaritalStatus\"/>    <code value=\"S\"/>    <display value=\"Single\"/>    </coding>    <text value=\"S\"/>    </maritalStatus>    </Patient>    </content>    <summary type=\"xhtml\">    <div xmlns=\"http://www.w3.org/1999/xhtml\">Patient HEATHER  CHEN (00234321): Female, born 19880811.&#x0A;      &#x0A;      &#x0A;    <br/>Address: 4491 HILL HAVEN DRIVE, NEW ORLEANS LA 70334</div>    </summary>    </entry></feed>";
    
    FHIRFeed *feed = [FHIRParser mapInfoFromXMLString:result];
    
    id value;
    
    FHIRPatient *patient = [(FHIRContent *)[(FHIREntry *)[feed.entry objectAtIndex:0] content] fhirObject];
    
    value = [(FHIRIdentifier *)[patient.identifier objectAtIndex:0] value];
    NSLog(@"Identifier Value: %@", value);
    XCTAssert([value isEqualToString:@"00234321"], @"Id should match.");
    
    value = [[(FHIRHumanName *)[patient.name objectAtIndex:0] family] objectAtIndex:0];
    NSLog(@"Family Name: %@", value);
    XCTAssert([value isEqualToString:@"CHEN"], @"Family Names should be equal.");
    
    value = [(FHIRCoding *)[patient.gender.coding objectAtIndex:0] code];
    NSLog(@"Gender Code: %@", value);
    XCTAssert([value isEqualToString:@"F"], @"Gender codes should be equal.");
    
    
    
    NSLog(@"Patient Feed Test - Local... finish");
}

- (void)testPatientListXML{

    NSLog(@"Patient List Test - XML... start");

    NSArray *list = @[@"example", @"1",@"10",@"100",@"101",@"102",@"103",@"104",@"105",@"animal",@"dicom",@"f001",@"f201",@"glossy",@"ihe-pcd",@"pat1",@"pat2",@"xcda",@"xds"];
    list = [NSArray new];

    for (NSString *patientId in list) {

        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://hl7connect.healthintersections.com.au/open/patient/%@?_format=xml", patientId]];
        NSString *result = [self requestUrl:url];

        FHIRPatient *patient = [FHIRParser mapInfoFromXMLString:result];
        NSString *serialized = [[[FHIRXmlWriter alloc] initWithFhirObject:patient] serializedString];
        if ([patientId isEqualToString:@"example"]){
            NSLog(@"STOP");
        }
        FHIRPatient *patient2 = [FHIRParser mapInfoFromXMLString:serialized];



        if (self.loud) {
            NSLog(@"%@", result);
            NSLog(@"%@", serialized);
        }
        FHIRHumanName *name = (FHIRHumanName *)[patient2.name objectAtIndex:0] ;
        NSLog(@"Patient: %@, %@ %@", patientId, [[name given] objectAtIndex:0], [[name family] objectAtIndex:0]);

        XCTAssert(patient2.identifier != nil, @"Something is not quite right.");
    }

    NSLog(@"Patient List Test - XML... finish");
}


- (NSString *) requestUrl:(NSURL *)url{
    
    __block BOOL complete = NO;
    __block NSString *result = nil;
    
    [self.post performRequestWithUrl:url withData:nil withCompletion:^(NSData *data) {
        
        result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        complete = YES;
        
    } withFailure:^(NSError *error) {
        
        NSLog(@"ERROR: %@",[error.userInfo valueForKey:NSLocalizedDescriptionKey]);
        complete = YES;
        
    }];
    
    while (complete == NO) {
        
        sleep(3);
    }
    
    return result;
}

@end
